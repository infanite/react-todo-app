import React, { Component } from "react";
import "./App.css";
import TodoInput from "./Components/TodoInput";
import TodoList from "./Components/TodoList";

// import 'bootstrap/dist/css/bootstrap.min.css';
import uuid from "react-uuid";

export default class App extends Component {

  state={
    items:[],
    id: uuid(),
    item:'',
    editItem: false,
  }

  inputHandleChange = (e) =>{
    this.setState({
      item: e.target.value
    })
  }
  
  handleSubmit = (e) =>{
    e.preventDefault();

    const newItem = {
      id: this.state.id,
      title: this.state.item,
    }

    const updatedItems = [...this.state.items, newItem];

    this.setState({ 
      items: updatedItems,
      item: "",
      id: uuid(),
      editItem: false
    })

  }

  handleClearList = () =>{
    this.setState({
      items: []
    })
  }

  handleDelete = (id) =>{
    const filteredItems = this.state.items.filter(item => item.id !== id);

    this.setState({ items: filteredItems })
  }

  handleEdit = (id) =>{
    // return the rest of the item other then trying to edit
    const filteredItems = this.state.items.filter((item) => item.id !== id);

    //find the item to edit
    const selectedItem = this.state.items.find(item => item.id === id);
    // console.log(selectedItem, "selectedItem")

    this.setState({ 
      items: filteredItems,
      item: selectedItem.title,
      editItem: true,
      id: id
    })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-10 mx-auto col-md-8 mt-4">
            <h3 className="text-capitalize text-center">Todo Input</h3>
            <TodoInput 
              item={this.state.item}
              inputHandleChange={ this.inputHandleChange }
              handleSubmit={ this.handleSubmit }
              editItem={ this.state.editItem }
            />
            <TodoList 
              items={this.state.items} 
              handleClearList={this.handleClearList} 
              handleDelete={this.handleDelete}
              handleEdit={this.handleEdit}
            />
          </div>
        </div>
      </div>
    );
  }
}
